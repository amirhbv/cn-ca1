import os
import socket
from threading import Thread, Lock

import status
from account_manager import AccountManager, AccountManagerException
from logger import Logger

MAX_BUFFER_SIZE = 2048


class FtpServer:

    def __init__(self, config):
        self.command_port = config.get('commandChannelPort', 8000)
        self.data_port = config.get('dataChannelPort', 8001)

        self.account_manager = AccountManager(
            config.get('users', []),
            config.get('accounting', {}),
            config.get('authorization', {}),
        )
        self.logger = Logger(config.get('logging', {}))
        self.home_dir = os.getcwd()
        self.data_sockets = {}
        self.clients = {}
        self.dir_lock = Lock()
        self.files_in_use = {}

        try:
            privileged_files = [os.path.abspath(
                file) for file in config['authorization'].get('files', [])]
        except KeyError:
            privileged_files = []
        self.privileged_files = privileged_files

    def _is_someone_in_there(self, path):
        return os.path.abspath(path) in [client.cwd for client in self.clients.values()]

    def _is_file_being_downloaded(self, path):
        return os.path.abspath(path) in self.files_in_use.values()

    def run(self):
        Thread(target=self._run_command_socket).start()
        Thread(target=self._run_data_socket).start()

    def _run_command_socket(self):
        command_socket = FtpServer._create_tcp_socket(
            host=socket._LOCALHOST, port=self.command_port)
        command_socket.listen(5)

        while True:
            client_socket, addr = command_socket.accept()
            ip, _ = addr
            if ip not in self.clients:
                handler = FtpClientHandler(
                    client_socket=client_socket, addr=addr, server=self)

                self.clients[ip] = handler
                Thread(target=handler.run).start()

        command_socket.close()

    def _run_data_socket(self):
        data_socket = FtpServer._create_tcp_socket(
            host=socket._LOCALHOST, port=self.data_port)
        data_socket.listen(5)

        while True:
            client_socket, addr = data_socket.accept()
            ip, port = addr
            self.data_sockets[ip] = client_socket

        data_socket.close()

    @staticmethod
    def _create_tcp_socket(host, port):
        tcp_socket = socket.socket(
            socket.AF_INET, socket.SOCK_STREAM)
        tcp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        tcp_socket.bind((host, port))

        return tcp_socket


class FtpClientHandler:

    def __init__(self, client_socket: socket.socket, addr, server: FtpServer):
        self.client_socket = client_socket
        self.addr = addr
        self.server = server
        self.username = None
        self.is_logged_in = False
        self.cwd = server.home_dir
        print(f'new connection from {self.addr}')

    def run(self):
        while True:
            data = self.client_socket.recv(2048).decode('utf-8')
            print(f'{self.addr} : {data}', end='')
            for line in data.split('\r\n')[:-1]:
                cmd = line.split(' ')[0].lower()
                arg = line[len(cmd) + 1:]
                flag = None
                path = None
                logged_out_user = None
                method = getattr(
                    self, f'command_{cmd}', self.command_not_found)

                if cmd == 'user':
                    result, resp = method(username=arg)
                elif cmd == 'pass':
                    result, resp = method(password=arg)
                else:
                    if self.is_logged_in:
                        if cmd == 'pasv':
                            result, resp = True, f'{self.server.data_port}'
                        else:
                            if cmd == 'quit':
                                logged_out_user = self.username

                            if len(arg):
                                for part in arg.split(' '):
                                    if part[0] == '-':
                                        flag = part
                                    else:
                                        path = part

                            result, resp = method(
                                username=self.username,
                                addr=self.addr,
                                path=self._normalize_path(path),
                                flag=flag,
                            )
                    else:
                        result, resp = self.login_error

                self.client_socket.sendall(resp.encode('utf-8'))

                self.server.logger.log(
                    cmd=cmd,
                    is_logged_in=self.is_logged_in,
                    username=self.username,
                    addr=self.addr,
                    user_cwd=self.cwd,
                    success=result,
                    path=self._normalize_path(path),
                    logged_out_user=logged_out_user,
                )
                logged_out_user = None

                if cmd == 'quit':
                    self.server.clients.pop(self.addr[0])
                    return

        self.client_socket.close()

    @property
    def auth_failed(self):
        return False, f'{status.LOGIN_ERROR} Invalid username or password.'

    @property
    def internal_error(self):
        return False, f'{status.INTERNAL_SERVER_ERROR} Error.'

    @property
    def login_error(self):
        return False, f'{status.NOT_LOGGED_IN} Need account for login.'

    def _normalize_path(self, path):
        if path:
            if path[0] == '/':
                return path

            return f'{self.cwd}/{path}'

    def command_user(self, username):
        result = self.server.account_manager.is_username_correct(username)
        resp = f'{status.USER_OK} Username okay, need password.' if result else self.auth_failed
        if result:
            self.username = username
        return result, resp

    def command_pass(self, password):
        try:
            result = self.server.account_manager.is_password_correct(
                self.username, password)
            resp = f'{status.LOGGED_IN} User logged in, proceed.' if result else self.auth_failed
        except AccountManagerException:
            result = False
            resp = f'{status.BAD_SEQUENCE} Bad sequence of commands.'

        if result:
            self.is_logged_in = True

        return result, resp

    def command_quit(self, **kwargs):
        self.username = None
        self.is_logged_in = False
        return True, f'{status.QUIT} Successful quit.'

    def command_not_found(self, **kwargs):
        return False, f'{status.SYNTAX_ERROR} Syntax error in parameters or arguments.'

    def command_pwd(self, **kwargs):
        return True, f'{status.PWD} {self.cwd}'

    def _mkdir(self, path, **kwargs):
        try:
            os.mkdir(path)
            return True, f'{status.MKD} {path} created.'
        except FileExistsError as exc:
            return False, f'{status.FILENAME_NOT_ALLOWED} this directory ({exc.filename}) already exists.'
        except FileNotFoundError as exc:
            return False, f'{status.FILE_UNAVAILABLE} the parent directory does not exist.'
        except:
            return self.internal_error

    def _touch(self, path, **kwargs):
        file_dir = path.split('/')
        filename = file_dir.pop(-1)
        if file_dir:
            dir_list = os.listdir('/'.join(file_dir))
            if filename in dir_list:
                return False, f'{status.FILENAME_NOT_ALLOWED} this directory ({path}) already exists.'
        open(path, 'w+').close()
        return True, f'{status.MKD} {path} created.'

    def command_mkd(self, path, flag, **kwargs):
        if not path:
            return False, f'{status.SYNTAX_ERROR} \'path\' argument is mandatory'

        self.server.dir_lock.acquire()
        if flag:
            if flag != '-i':
                result, resp = self.command_not_found()
            else:
                result, resp = self._touch(path)
        else:
            result, resp = self._mkdir(path)
        self.server.dir_lock.release()
        return result, resp

    def command_rmd(self, path, flag, **kwargs):
        if not path:
            return False, f'{status.SYNTAX_ERROR} \'path\' argument is mandatory'

        if path in self.server.privileged_files:
            if not self.server.account_manager.can_access_path(
                    username=self.username,
                    path=path,
                    privileged=True,
            ):
                return False, f'{status.FILE_UNAVAILABLE} File not available.'

        if flag:
            if flag != '-f':
                return self.command_not_found()

            if self.server._is_someone_in_there(path):
                return False, f'{status.FILE_UNAVAILABLE} this directory is being used by another user.'

            try:
                self.server.dir_lock.acquire()
                os.rmdir(path)
                result, resp = True, f'{status.RMD} {path} deleted.'
            except FileNotFoundError as exc:
                result, resp = False, f'{status.FILE_UNAVAILABLE} this directory ({exc.filename}) does not exist.'
            except OSError as exc:
                result, resp = False, f'{status.DIR_NOT_EMPTY} {exc.filename} {exc.strerror}'
            except:
                result, resp = False, f'{status.INTERNAL_SERVER_ERROR} Error.'
            finally:
                self.server.dir_lock.release()
                return result, resp
        else:
            if self.server._is_file_being_downloaded(path):
                return False, f'{status.FILE_UNAVAILABLE} this file is being used by another user.'

            try:
                self.server.dir_lock.acquire()
                os.remove(path)
                result, resp = True, f'{status.RMD} {path} deleted.'
            except IsADirectoryError as exc:
                result, resp = False, f'{status.SYNTAX_ERROR} This path ({exc.filename}) belongs to a directory.'
            except FileNotFoundError as exc:
                result, resp = False, f'{status.FILE_UNAVAILABLE} this file ({exc.filename}) does not exist.'
            except:
                result, resp = self.internal_error
            finally:
                self.server.dir_lock.release()
                return result, resp

    def command_cwd(self, path, **kwargs):
        try:
            self.server.dir_lock.acquire()
            if path:
                os.chdir(path)
            else:
                os.chdir(self.server.home_dir)

            self.cwd = os.getcwd()
            result, resp = True, f'{status.CWD} Successful change.'
        except FileNotFoundError as exc:
            result, resp = False, f'{status.FILE_UNAVAILABLE} this directory ({exc.filename}) does not exist.'
        except:
            result, resp = self.internal_error
        finally:
            self.server.dir_lock.release()
            return result, resp

    def command_help(self, **kwargs):
        with open('./help.txt', 'r') as f:
            help_text = f.read()
        return True, f'{status.HELP}\n {help_text}'

    def command_dl(self, path, **kwargs):
        ip, _ = self.addr
        if not path:
            return False, f'{status.SYNTAX_ERROR} \'path\' argument is mandatory'

        try:
            if not self.server.account_manager.can_download_path(
                    username=self.username,
                    path=path,
                    privileged=path in self.server.privileged_files,
                    file_size=os.path.getsize(path),
            ):
                return False, f'{status.FILE_UNAVAILABLE} File not available.'
        except FileNotFoundError as exc:
            return False, f'{status.FILE_UNAVAILABLE} this file ({exc.filename}) does not exist.'

        if not os.path.isfile(path):
            return False, f'{status.SYNTAX_ERROR} Directories can\'t be downloaded.'
        data_socket = self.server.data_sockets.get(ip, None)
        if not data_socket:
            return False, f'{status.CANT_OPEN_DATA_CONNECTION} data connection was not found! Please connect to the ' \
                          f'data port supplied to your client before initiating a download sequence'

        try:
            self.server.files_in_use[ip] = os.path.abspath(path)
            with open(path, 'rb') as fin:
                bytes_to_send = fin.read(MAX_BUFFER_SIZE)
            self.server.files_in_use.pop(ip)
            data_socket.sendall(bytes_to_send)
            result, resp = True, f'{status.DL} Download successful.'
        except FileNotFoundError as exc:
            result, resp = False, f'{status.FILE_UNAVAILABLE} this file ({exc.filename}) does not exist.'
        except Exception as exc:
            print(exc)
            result, resp = self.internal_error
        finally:
            return result, resp

    def command_list(self, **kwargs):
        ip, _ = self.addr
        data_socket = self.server.data_sockets.get(ip, None)
        if not data_socket:
            return False, f'{status.CANT_OPEN_DATA_CONNECTION} data connection was not found! Please connect to the ' \
                f'data port supplied to your client before initiating the LIST command'
        dir_list = '/'.join(os.listdir(self.cwd))
        data_socket.sendall(dir_list.encode('utf-8'))
        return True, f'{status.LIST} List transfer done.'


class FtpServerException(Exception):
    pass
