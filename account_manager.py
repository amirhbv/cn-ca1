import base64
import os
import time
import socket
from threading import Lock


class AccountManager:
    def __init__(self, users, accounting, authorization):
        self.users = {}
        for user in users:
            self.users[user['user']] = {'password': user['password']}

        if not accounting or not accounting.get('enable', None):
            self.accounting_enabled = False
        else:
            self.accounting_enabled = True
            self.accounting_threshold = accounting.get('threshold')
            for user in accounting['users']:
                if user['user'] not in self.users:
                    raise ValueError(
                        'accounting data for non-existing user given!')
                self.users[user['user']]['size'] = int(user['size'])
                self.users[user['user']]['email'] = user['email']
                self.users[user['user']]['alert'] = user['alert']

        if not authorization or not authorization.get('enable', False):
            self.authorization_enabled = False
        else:
            self.authorization_enabled = True
            for user in authorization['admins']:
                if user not in self.users:
                    raise ValueError(
                        'authorization data for non-existing user given!')
                self.users[user]['admin'] = True
        self.users_lock = Lock()

    def is_username_correct(self, username):
        return username in self.users

    def is_password_correct(self, username, password):
        if not self.is_username_correct(username):
            raise AccountManagerException('There is no such user')
        return password == self.users[username]['password']

    def can_download_path(self, username, path, file_size=None, privileged=False):
        self.users_lock.acquire()

        if not self.is_username_correct(username):
            self.users_lock.release()
            raise AccountManagerException('There is no such user')

        if not self.can_access_path(username, path, privileged):
            self.users_lock.release()
            return False

        if self.accounting_enabled:
            if not file_size:
                self.users_lock.release()
                raise AccountManagerException(
                    'File size should be given when accounting is enabled')
            else:
                if file_size > self.users[username]['size']:
                    self.users_lock.release()
                    return False

                self.users[username]['size'] -= file_size
                if self.users[username]['size'] < self.accounting_threshold and self.users[username]['alert']:
                    AccountManager.send_email(self.users[username]['email'])

        self.users_lock.release()
        return True

    def can_access_path(self, username, path, privileged=False):
        return not privileged or not self.authorization_enabled or self.users[username].get('admin')

    @staticmethod
    def send_email(recepient):
        mailserver = ("mail.ut.ac.ir", 25)
        clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        clientSocket.connect(mailserver)
        clientSocket.recv(1024)
        heloCommand = 'EHLO mail\r\n'
        clientSocket.send(heloCommand.encode())
        clientSocket.recv(1024)
        username = os.getenv('UT_MAIL_USERNAME')
        password = os.getenv('UT_MAIL_PASSWORD')
        base64_str = base64.b64encode(
            ("\x00"+username+"\x00"+password).encode())
        authMsg = "AUTH PLAIN ".encode()+base64_str+"\r\n".encode()
        clientSocket.send(authMsg)
        clientSocket.recv(1024)
        mailFrom = f"MAIL FROM: <{username}@ut.ac.ir>\r\n"
        clientSocket.send(mailFrom.encode())
        clientSocket.recv(1024)
        rcptTo = f"RCPT TO: <{recepient}>\r\n"
        clientSocket.send(rcptTo.encode())
        clientSocket.recv(1024)
        data = "DATA\r\n"
        clientSocket.send(data.encode())
        clientSocket.recv(1024)
        subject = "Subject: FTP traffic limit warning\r\n\r\n"
        clientSocket.send(subject.encode())
        date = time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime())
        date = date + "\r\n\r\n"
        clientSocket.send(date.encode())
        msg = "\r\n Your traffic limit has fallen below the threshold!"
        endmsg = "\r\n.\r\n"
        clientSocket.send(msg.encode())
        clientSocket.send(endmsg.encode())
        clientSocket.recv(1024)
        quit = "QUIT\r\n"
        clientSocket.send(quit.encode())
        clientSocket.recv(1024)
        clientSocket.close()


class AccountManagerException(Exception):
    pass
