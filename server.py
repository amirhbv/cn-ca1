import json
from dotenv import load_dotenv

from ftp_server import FtpServer

load_dotenv()
with open('./config.json', 'r') as config_file:
    config = json.load(config_file)
    FtpServer(config).run()
