import argparse

from ftp_client import FtpClient

parser = argparse.ArgumentParser(description='FTP Client.')
parser.add_argument('-P', '--port', type=int, required=True,
                    help="FTP Server command port")
parser.add_argument('-u', '--username', type=str,
                    required=True, help="Client username")
parser.add_argument('-p', '--password', type=str,
                    required=True, help="Client password")

args = parser.parse_args()


FtpClient(
    server_command_port=args.port,
).run(
    username=args.username,
    password=args.password,
)
