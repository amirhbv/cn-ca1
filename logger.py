import os
import threading
from datetime import datetime


class Logger(object):

    def __init__(self, config):
        if not config or not config.get('enable', None):
            self.logging_enabled = False
        else:
            self.logging_enabled = True
            file_path = config.get('path', None)
            self.lock = threading.Lock()
            if file_path:
                self.path = os.path.abspath(file_path)
                if not os.path.exists(self.path):
                    open(self.path, 'w+').close()
            else:
                raise LoggerException(
                    '\'path\' key is mandatory when logging is enabled')

    def log(self, is_logged_in, **kwargs):
        cmd = kwargs.get('cmd')
        logged_out_user = kwargs.get('logged_out_user', None)
        if self.logging_enabled:
            method = getattr(
                self,
                f'_log_{cmd}' if is_logged_in or cmd in [
                    'user', 'pass'] or logged_out_user else '_log_not_logged_in',
                self._log_not_found)

            method(**kwargs)

    def _write_log(self, msg):
        date_str = datetime.now().strftime('%Y-%m-%d %H:%M')

        self.lock.acquire()
        with open(self.path, 'a') as fout:
            fout.write(f'[{date_str}] {msg}')
        self.lock.release()

    def _create_log_msg(self, username, cmd, success, path=None):
        log_msgs = {
            'success_resp': f'User: {username}, {cmd} executed successfully.',
            'success_path': f' The command was executed on: {path}',
            'failed_resp': f'User: {username}, {cmd} execution failed.',
            'failed_path': f' The command failed to execute on: {path}',
        }
        resp = log_msgs.get(
            'success_resp') if success else log_msgs.get('failed_resp')

        if path:
            resp += (log_msgs.get('success_path')
                     if success else log_msgs.get('failed_path'))

        return resp + '\n'

    def _log_common_cmds(self, **kwargs):
        success = kwargs.get('success', False)
        username = kwargs.get('username', None)
        path = kwargs.get('path', None)
        cmd = kwargs.get('cmd', None)
        if cmd == 'list':
            path = kwargs.get('user_cwd')

        self._write_log(
            self._create_log_msg(
                username=username,
                success=success,
                cmd=cmd.upper(),
                path=path,
            )
        )

    _log_pasv = _log_common_cmds
    _log_pwd = _log_common_cmds
    _log_mkd = _log_common_cmds
    _log_rmd = _log_common_cmds
    _log_cwd = _log_common_cmds
    _log_help = _log_common_cmds
    _log_dl = _log_common_cmds
    _log_list = _log_common_cmds

    def _log_user(self, **kwargs):
        success = kwargs.get('success', False)
        username = kwargs.get('username', None)
        addr = kwargs.get('addr', None)
        if success:
            log_msg = f'User: {username} entered username successfully from addr: {addr}.\n'
        else:
            log_msg = f'Failed login attempt from addr {addr}, username not found.\n'

        self._write_log(log_msg)

    def _log_pass(self, **kwargs):
        success = kwargs.get('success', False)
        username = kwargs.get('username', None)
        addr = kwargs.get('addr', None)
        if success:
            log_msg = f'User: {username} logged in successfully with addr: {addr}.\n'
        else:
            if username:
                log_msg = f'Failed login attempt from addr {addr} for username {username}, wrong password.\n'
            else:
                log_msg = f'Password entered from addr {addr} without a username.\n'

        self._write_log(log_msg)

    def _log_quit(self, **kwargs):
        username = kwargs.get('logged_out_user')

        self._write_log(f'User: {username} logged out.\n')

    def _log_not_found(self, **kwargs):
        username = kwargs.get('username', None)
        cmd = kwargs.get('cmd', None)

        self._write_log(
            f'User: {username} entered unknown command ({cmd}).\n')

    def _log_not_logged_in(self, **kwargs):
        cmd = kwargs.get('cmd', None)
        addr = kwargs.get('addr', None)

        self._write_log(
            f'Command {cmd} entered without logging in first, addr: {addr}\n')


class LoggerException(Exception):
    pass
