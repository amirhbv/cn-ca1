import os
import socket

import status


class FtpClient:
    BUFFER_SIZE = 2048

    def __init__(self, server_command_port):
        self.command_port = server_command_port

    @staticmethod
    def _encode_msg(msg):
        return f'{msg}\r\n'.encode('utf-8')

    def run(self, username, password):
        command_socket = socket.socket()
        try:
            command_socket.connect((socket._LOCALHOST, self.command_port))
        except:
            print('Invalid server port')
            return

        command_socket.sendall(FtpClient._encode_msg(f'USER {username}'))
        resp = command_socket.recv(FtpClient.BUFFER_SIZE).decode('utf-8')
        print(resp)
        resp_status = resp.split(' ')[0]
        if int(resp_status) != status.USER_OK:
            print('Invalid username')
            return

        command_socket.sendall(FtpClient._encode_msg(f'PASS {password}'))
        resp = command_socket.recv(FtpClient.BUFFER_SIZE).decode('utf-8')
        print(resp)
        resp_status = resp.split(' ')[0]
        if int(resp_status) != status.LOGGED_IN:
            print('Invalid username or password')
            return

        command_socket.sendall(FtpClient._encode_msg('PASV'))
        resp = command_socket.recv(FtpClient.BUFFER_SIZE).decode('utf-8')
        print(f'Data socket will connect to port: {resp}')
        data_port = int(resp)

        data_socket = socket.socket()
        try:
            data_socket.connect((socket._LOCALHOST, data_port))
        except:
            print('Invalid data port')
            return

        while True:
            line = input()
            command_socket.sendall(FtpClient._encode_msg(line))

            cmd = line.split(' ')[0].lower()
            arg = line[len(cmd) + 1:]

            resp = command_socket.recv(FtpClient.BUFFER_SIZE).decode('utf-8')
            resp_status = resp.split(' ')[0]
            print(resp)

            if cmd == 'quit':
                data_socket.close()
                command_socket.close()
                return

            if resp_status[0] == '2' and cmd in ['list', 'dl']:
                resp_data = data_socket.recv(
                    FtpClient.BUFFER_SIZE).decode('utf-8')
                if cmd == 'list':
                    print('\t' + '\n\t'.join(resp_data.split('/')))
                elif cmd == 'dl':
                    with open(os.path.abspath(arg), 'w') as fout:
                        fout.write(resp_data)
